import { render, screen } from '@testing-library/react';
import Article from './Article';

describe('Article component', () => {
  it('renders section headline', () => {
    render(<Article />);
    const headlineElement = screen.getByText(/This is the Section/i);
    expect(headlineElement).toBeInTheDocument();
  });

  it('renders "Read more" button', () => {
    render(<Article />);
    const buttonElement = screen.getByRole('button', { name: 'Read more' });
    expect(buttonElement).toBeInTheDocument();
  });

  it('renders article text', () => {
    render(<Article />);
    const articleTextElement = screen.getByText(
      /Lorem ipsum dolor sit amet consectetur /i
    );
    expect(articleTextElement).toBeInTheDocument();
  });
});
