import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Card from './Card';

const mockData = {
  id: 1,
  avatar: 'https://example.com/avatar.png',
  firstName: 'John',
  lastName: 'Doe',
  position: 'Software Engineer',
};

describe('Card', () => {
  it('renders correctly', () => {
    render(
      <MemoryRouter>
        <Card item={mockData} />
      </MemoryRouter>
    );

    const avatar = screen.getByAltText('avatar');
    expect(avatar).toHaveAttribute('src', 'https://example.com/avatar.png');

    const fullName = screen.getByText(/John Doe/i);
    expect(fullName).toBeInTheDocument();

    const position = screen.getByText('Software Engineer');
    expect(position).toBeInTheDocument();
  });
});
