import { render, screen } from '@testing-library/react';
import Footer from './Footer';

describe('Footer component', () => {
  it('renders section headline', () => {
    render(<Footer />);
    const headlineElement = screen.getByText(/project/i);
    expect(headlineElement).toBeInTheDocument();
  });

  it('renders footer text', () => {
    render(<Footer />);
    const articleTextElement = screen.getByText(/123 Street/i);
    expect(articleTextElement).toBeInTheDocument();
  });
});
