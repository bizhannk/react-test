import { render, screen } from '@testing-library/react';
import Hero from './Hero';

describe('Hero component', () => {
  it('renders section headline', () => {
    render(<Hero />);
    const logoImg = screen.getByAltText(/logo/i);
    expect(logoImg).toBeInTheDocument();
  });

  it('renders heading', () => {
    render(<Hero />);
    const headingElement = screen.getByText(/Your Headline/i);
    expect(headingElement).toBeInTheDocument();
  });

  it('renders subheading text', () => {
    render(<Hero />);
    const subheadingElement = screen.getByText(/Lorem ipsum dolor sit/i);
    expect(subheadingElement).toBeInTheDocument();
  });
});
