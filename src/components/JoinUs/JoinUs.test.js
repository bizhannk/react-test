import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';
import JoinUs from './JoinUs';
import { setIsDisabled } from '../../features/joinUs/joinUsSlice';

jest.mock('react-redux');

describe('JoinUs', () => {
  beforeEach(() => {
    useSelector.mockImplementation((selector) =>
      selector({
        joinUs: {
          isSubscribed: false,
          isDisabled: false,
        },
      })
    );
  });

  it('should render the title and description', () => {
    render(<JoinUs />);
    expect(screen.getByText('Join Our Program')).toBeInTheDocument();
    expect(
      screen.getByText(
        'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      )
    ).toBeInTheDocument();
  });

  it('should render the email input and subscribe button', () => {
    render(<JoinUs />);
    const emailInput = screen.getByPlaceholderText('Email');
    const subscribeButton = screen.getByText('Subscribe');
    expect(emailInput).toBeInTheDocument();
    expect(subscribeButton).toBeInTheDocument();
  });

  it('should submit the form and call the subscribe function', async () => {
    const dispatchMock = jest.fn();
    useDispatch.mockReturnValue(dispatchMock);
    render(<JoinUs />);
    const emailInput = screen.getByPlaceholderText('Email');
    const subscribeButton = screen.getByText('Subscribe');
    fireEvent.change(emailInput, { target: { value: 'test@example.com' } });
    fireEvent.click(subscribeButton);
    expect(dispatchMock).toHaveBeenCalledWith(setIsDisabled(true));
    await waitFor(() => {
      expect(dispatchMock).not.toHaveBeenCalledWith(setIsDisabled(false));
    });
  });
});
