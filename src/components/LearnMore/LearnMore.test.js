import { render, screen } from '@testing-library/react';
import LearnMore from './LearnMore';

describe('LearnMore component', () => {
  it('renders heading', () => {
    render(<LearnMore />);
    const headingElement = screen.getByText(/Learn more/i);
    expect(headingElement).toBeInTheDocument();
  });

  it('renders subheading text', () => {
    render(<LearnMore />);
    const subheadingElement = screen.getByText(/Duis aute irure dolor /i);
    expect(subheadingElement).toBeInTheDocument();
  });
});
