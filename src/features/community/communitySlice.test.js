import communityReducer, {
  fetchCommunityData,
  setIsHidden,
} from './communitySlice';

describe('communitySlice', () => {
  const communityData = [{ id: 1, name: 'John' }, { id: 2, name: 'Jane' }];
  const error = 'Unable to fetch community data';


  describe('setIsHidden', () => {
    it('sets isHidden to true', () => {
      const state = { isHidden: false };

      const newState = communityReducer(state, setIsHidden(true));

      expect(newState).toEqual({ isHidden: true });
    });

    it('sets isHidden to false', () => {
      const state = { isHidden: true };

      const newState = communityReducer(state, setIsHidden(false));

      expect(newState).toEqual({ isHidden: false });
    });
  });

  describe('communityReducer', () => {
    it('handles fetchCommunityData.pending', () => {
      const state = { status: 'idle' };
      const action = fetchCommunityData.pending();

      const newState = communityReducer(state, action);

      expect(newState.status).toBe('loading');
    });

    it('handles fetchCommunityData.fulfilled', () => {
      const state = { status: 'loading', data: [], error: null };
      const action = fetchCommunityData.fulfilled(communityData);

      const newState = communityReducer(state, action);

      expect(newState.status).toBe('succeeded');
      expect(newState.data).toEqual(communityData);
      expect(newState.error).toBe(null);
    });

    it('handles fetchCommunityData.rejected', () => {
      const state = { status: 'loading', data: [], error: null };
      const action = fetchCommunityData.rejected({ message: error });

      const newState = communityReducer(state, action);

      expect(newState.status).toBe('failed');
      expect(newState.data).toEqual([]);
      expect(newState.error).toBe(error);
    });

    it('handles setIsHidden', () => {
      const state = { isHidden: false };
      const action = setIsHidden(true);

      const newState = communityReducer(state, action);

      expect(newState.isHidden).toBe(true);
    });
  });
});
