import joinUsReducer, {
  setIsSubscribed,
  setIsDisabled,
} from './joinUsSlice';

describe('joinUsSlice', () => {
  test('should set isSubscribed to true', () => {
    const initialState = { isSubscribed: false };
    const newState = joinUsReducer(initialState, setIsSubscribed(true));

    expect(newState.isSubscribed).toBe(true);
  });

  test('should set isDisabled to false', () => {
    const initialState = { isDisabled: true };
    const newState = joinUsReducer(initialState, setIsDisabled(false));

    expect(newState.isDisabled).toBe(false);
  });
});
