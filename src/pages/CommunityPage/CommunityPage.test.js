import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';
import { setIsHidden } from '../../features/community/communitySlice';
import CommunityPage from './CommunityPage';

jest.mock('react-redux');

describe('Community page', () => {
  beforeEach(() => {
    useSelector.mockImplementation((selector) =>
      selector({
        community: {
          isHidden: false,
        },
      })
    );
  });

  it('should change section visibility', async () => {
    const dispatchMock = jest.fn();
    useDispatch.mockReturnValue(dispatchMock);
    render(<CommunityPage />);
    const button = screen.getByText(/section/i);
    fireEvent.click(button);
    expect(dispatchMock).toHaveBeenCalledWith(setIsHidden(true));
  });
});
