import React from 'react';
import Hero from '../../components/Hero/Hero';
import JoinUs from '../../components/JoinUs/JoinUs';
import LearnMore from '../../components/LearnMore/LearnMore';
import Article from '../../components/Article/Article';

const HomePage = () => {
  return (
    <>
      <Hero />
      <Article />
      <LearnMore />
      <JoinUs />
    </>
  );
};

export default HomePage;
