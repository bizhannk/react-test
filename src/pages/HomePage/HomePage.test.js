import { render, screen } from '@testing-library/react';
import HomePage from './HomePage';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

// import { MemoryRouter, Route, Routes } from 'react-router-dom';
// const FakeComponent = () => <div>fake text</div>;

// describe('LearnMore component', () => {
//   it('renders subheading text', () => {
//     render(
//       <MemoryRouter initialEntries={['/']}>
//         <Routes>
//           <Route element={<CheckAuth />}>
//             <Route path="/" element={<FakeComponent />} />
//           </Route>
//         </Routes>
//       </MemoryRouter>
//     );
//     expect(screen.queryByText('fake text')).not.toBeInTheDocument();
//   });
// });

describe('Homepage component', () => {
  it('renders sub-components correctly', () => {
    render(
      <Provider store={store}>
        <HomePage />
      </Provider>
    );

    const hero = screen.getByTestId('hero');
    const article = screen.getByTestId('article');
    const learnMore = screen.getByTestId('learn-more');
    const joinUs = screen.getByTestId('join-us');

    expect(hero).toBeInTheDocument();
    expect(article).toBeInTheDocument();
    expect(learnMore).toBeInTheDocument();
    expect(joinUs).toBeInTheDocument();
  });
});
