import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import NotFoundPage from './NotFoundPage';

describe('NotFoundPage', () => {
  it('renders page not found message', () => {
    render(
      <MemoryRouter>
        <NotFoundPage />
      </MemoryRouter>
    );
    const heading = screen.getByText('Page Not Found');
    expect(heading).toBeInTheDocument();
    const subheading = screen.getByText(
      "Looks like you've followed a broken link or entered URL that dosen't exist on this site"
    );
    expect(subheading).toBeInTheDocument();
    const backLink = screen.getByRole('link', { name: /back to our site/i });
    expect(backLink).toHaveAttribute('href', '/');
  });
});
